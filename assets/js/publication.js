$('#nova-publicacao').on('submit', (event) => {
    event.preventDefault();

    $.ajax({
        url: "/publications",
        method: "POST",
        data: {
            title: $("#titulo").val(),
            description: $("#conteudo").val()
        }
    }).done(() => {
        location.reload()
    }).fail(() => {
        console.log("erro")
    })
})

$(document).on('click', '.curtir-publicacao', (event) => {
    event.preventDefault()

    const elementoClicado = $(event.target)
    const publicationID = elementoClicado.closest('div').data('publicacao-id')

    elementoClicado.prop('disabled', true);
    $.ajax({
        url: `/publications/${publicationID}/like`,
        method: "POST"
    }).done(() => {
        const countLikes = elementoClicado.next('span')
        const totalLikes = parseInt(countLikes.text())
        countLikes.text(totalLikes + 1);

        elementoClicado.addClass('descurtir-publicacao');
        elementoClicado.addClass('text-danger');
        elementoClicado.removeClass('curtir-publicacao');

    }).fail(() => {
        console.log("Erro")
    }).always(() => {
        elementoClicado.prop('disabled', false);
    })
})

$(document).on('click', '.descurtir-publicacao', (event) => {
    event.preventDefault()

    const elementoClicado = $(event.target)
    const publicationID = elementoClicado.closest('div').data('publicacao-id')

    elementoClicado.prop('disabled', true);
    $.ajax({
        url: `/publications/${publicationID}/dislike`,
        method: "POST"
    }).done(() => {
        const countLikes = elementoClicado.next('span')
        const totalLikes = parseInt(countLikes.text())
        countLikes.text(totalLikes - 1);

        elementoClicado.removeClass('descurtir-publicacao');
        elementoClicado.removeClass('text-danger');
        elementoClicado.addClass('curtir-publicacao');

    }).fail(() => {
        console.log("Erro")
    }).always(() => {
        elementoClicado.prop('disabled', false);
    })
})

$(document).on('click', '#update-publication', () => {
    $("#update-publication").prop('disabled', true);
    const publicationID = $("#update-publication").data('publication-id')
    $.ajax({
        url: `/publications/${publicationID}`,
        method: "PUT",
        data: {
            title: $("#title").val(),
            description: $("#description").val()
        }
    }).done(() => {
        Swal.fire(
            'Sucesso',
            'Publicação atualizada com sucesso',
            'success'
        ).then(() => {
            window.location = '/home'
        })
    }).fail(() => {
        console.log("erro")
    }).always(() => {
        $("#update-publication").prop('disabled', false);
    })
})


$(document).on('click', '.delete-publication', (event) => {
    event.preventDefault()

    Swal.fire({
        title: 'Atenção!',
        text: 'Tem certeza que deseja remover essa publicação?',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        icon: 'warning'
    }).then((confirmed) => {
        if (!confirmed.value) return
        const elementoClicado = $(event.target)
        const publication = elementoClicado.closest('div')
        const publicationID = publication.data('publicacao-id')

        elementoClicado.prop('disabled', true);
        $.ajax({
            url: `/publications/${publicationID}`,
            method: "DELETE"
        }).done(() => {
            publication.fadeOut('slow', () => {
                $(this).remove();
            })

        }).fail(() => {
            console.log("Erro")
        })
    })
})