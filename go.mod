module web-app

go 1.13

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/securecookie v1.1.1
	github.com/joho/godotenv v1.4.0
	golang.org/x/text v0.3.7
	honnef.co/go/tools v0.3.2
)
