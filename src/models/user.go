package models

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"
	"web-app/src/config"
	"web-app/src/requests"
)

type User struct {
	ID           uint64        `json:"id"`
	Name         string        `json:"name"`
	Email        string        `json:"email"`
	Username     string        `json:"username"`
	CreatedAt    time.Time     `json:"created_at"`
	Followers    []User        `json:"followers"`
	Following    []User        `json:"following"`
	Publications []Publication `json:"publications"`
}

func SearchUserComplete(profileID uint64, r *http.Request) (User, error) {
	userChannel := make(chan User)
	followersChannel := make(chan []User)
	followingChannel := make(chan []User)
	publicationChannel := make(chan []Publication)

	go SearchUserChannel(userChannel, profileID, r)
	go SearchFollowersChannel(followersChannel, profileID, r)
	go SearchFollowingChannel(followingChannel, profileID, r)
	go SearchPublicationChannel(publicationChannel, profileID, r)

	var (
		user         User
		followers    []User
		followings   []User
		publications []Publication
	)

	for i := 0; i < 4; i++ {
		select {
		case userLoaded := <-userChannel:
			if userLoaded.ID == 0 {
				return User{}, errors.New("erro ao buscar o usuário")
			}
			user = userLoaded
		case followersLoaded := <-followersChannel:
			if followersLoaded == nil {
				return User{}, errors.New("erro ao buscar seguidores")
			}
			followers = followersLoaded
		case followingLoaded := <-followingChannel:
			if followingLoaded == nil {
				return User{}, errors.New("erro ao buscar quem o usuário está seguindo")
			}
			followings = followingLoaded
		case publicationsLoaded := <-publicationChannel:
			if publicationsLoaded == nil {
				return User{}, errors.New("erro ao buscar publicações")

			}
			publications = publicationsLoaded
		}
	}

	user.Followers = followers
	user.Following = followings
	user.Publications = publications
	return user, nil
}

func SearchUserChannel(channel chan<- User, profileID uint64, r *http.Request) {
	url := fmt.Sprintf("%s/users/%d", config.ApiURL, profileID)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodGet, url, nil)
	if err != nil {
		channel <- User{}
		return
	}
	defer response.Body.Close()

	var user User
	if err = json.NewDecoder(response.Body).Decode(&user); err != nil {
		channel <- User{}
		return
	}

	channel <- user
}

func SearchFollowersChannel(channel chan<- []User, profileID uint64, r *http.Request) {
	url := fmt.Sprintf("%s/followers/%d", config.ApiURL, profileID)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodGet, url, nil)
	if err != nil {
		channel <- nil
		return
	}
	defer response.Body.Close()

	var followers []User
	if err = json.NewDecoder(response.Body).Decode(&followers); err != nil {
		channel <- nil
		return
	}

	if followers == nil {
		channel <- make([]User, 0)
		return
	}

	channel <- followers
}

func SearchFollowingChannel(channel chan<- []User, profileID uint64, r *http.Request) {
	url := fmt.Sprintf("%s/followings/%d", config.ApiURL, profileID)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodGet, url, nil)
	if err != nil {
		channel <- nil
		return
	}
	defer response.Body.Close()

	var following []User
	if err = json.NewDecoder(response.Body).Decode(&following); err != nil {
		channel <- nil
		return
	}

	if following == nil {
		channel <- make([]User, 0)
		return
	}

	channel <- following
}

func SearchPublicationChannel(channel chan<- []Publication, profileID uint64, r *http.Request) {
	url := fmt.Sprintf("%s/users/%d/publications", config.ApiURL, profileID)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodGet, url, nil)
	if err != nil {
		channel <- nil
		return
	}
	defer response.Body.Close()

	var publications []Publication
	if err = json.NewDecoder(response.Body).Decode(&publications); err != nil {
		channel <- nil
		return
	}

	if publications == nil {
		channel <- make([]Publication, 0)
		return
	}

	channel <- publications
}
