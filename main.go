package main

import (
	"fmt"
	"log"
	"net/http"
	"web-app/src/config"
	"web-app/src/cookies"
	"web-app/src/router"
	"web-app/src/utils"
)

// func init() {
// 	hashKey := hex.EncodeToString(securecookie.GenerateRandomKey(16))
// 	fmt.Println(hashKey)
// 	blockKey := hex.EncodeToString(securecookie.GenerateRandomKey(16))
// 	fmt.Println(blockKey)
// }

func main() {
	config.Loading()
	cookies.LoadingCookie()
	utils.LoadingTemplates()
	fmt.Println("Rodando web app")
	r := router.Build()
	log.Fatal(http.ListenAndServe(":3000", r))
}
