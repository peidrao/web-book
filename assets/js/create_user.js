$('#form-signup').on('submit', (event) => {
    event.preventDefault();

    if ($("#password").val() != $("#re-password").val()) {
        Swal.fire('Ops...', 'As senhas não coinciden', 'error')
        return;
    }

    $.ajax({
        url: "/users",
        method: "POST",
        data: {
            email: $("#email").val(),
            name: $("#name").val(),
            username: $("#username").val(),
            password: $("#password").val(),
        }
    }).done((e) => {
        Swal.fire('Sucesso', 'Usuário cadastrado com sucesso!', 'success').then(() => {
            $.ajax({
                url: '/login',
                method: "POST",
                data: {
                    email: $("#email").val(),
                    password: $("#password").val(),
                }
            }).done(() => {
                window.location = '/home'
            })
        })
    }).fail((err) => {
        Swal.fire('Ops...', 'Houve um erro ao cadastrar um usuário', 'error')
    })
})