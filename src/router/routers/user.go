package routers

import (
	"net/http"
	"web-app/src/controllers"
)

var routersUsers = []Route{
	{
		URI:                     "/users",
		Method:                  http.MethodPost,
		Function:                controllers.CreateUser,
		IsRequestAuthentication: false,
	},
	{
		URI:                     "/logout",
		Method:                  http.MethodGet,
		Function:                controllers.Logout,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/search-profiles",
		Method:                  http.MethodGet,
		Function:                controllers.SearchUsers,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/profile/{profileID}",
		Method:                  http.MethodGet,
		Function:                controllers.GetProfile,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/profile/{profileID}/follow",
		Method:                  http.MethodPost,
		Function:                controllers.FollowProfile,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/profile/{profileID}/unfollow",
		Method:                  http.MethodPost,
		Function:                controllers.UnFollowProfile,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/profile",
		Method:                  http.MethodGet,
		Function:                controllers.GetProfileLogged,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/edit-user",
		Method:                  http.MethodGet,
		Function:                controllers.GetPageProfileEdit,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/edit-user",
		Method:                  http.MethodPut,
		Function:                controllers.UpdateProfile,
		IsRequestAuthentication: true,
	},
		{
		URI:                     "/update-password",
		Method:                  http.MethodGet,
		Function:                controllers.GetPagePasswordEdit,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/update-password",
		Method:                  http.MethodPost,
		Function:                controllers.UpdatePassword,
		IsRequestAuthentication: true,
	},{
		URI:                     "/delete-password",
		Method:                  http.MethodDelete,
		Function:                controllers.DeleteProfile,
		IsRequestAuthentication: true,
	},
}
