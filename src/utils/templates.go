package utils

import (
	"html/template"
	"net/http"
)

var templates *template.Template

func LoadingTemplates() {
	templates = template.Must(template.ParseGlob("views/*.html"))
	templates = template.Must(templates.ParseGlob("views/partials/*.html"))
}

func ExecuteTemplate(w http.ResponseWriter, template string, context interface{}) {
	templates.ExecuteTemplate(w, template, context)
}
