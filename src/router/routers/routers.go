package routers

import (
	"net/http"
	"web-app/src/middlewares"

	"github.com/gorilla/mux"
)

type Route struct {
	URI                     string
	Method                  string
	Function                func(http.ResponseWriter, *http.Request)
	IsRequestAuthentication bool
}

func Configure(r *mux.Router) *mux.Router {
	routers := routersLogin
	routers = append(routers, routersUsers...)
	routers = append(routers, routersPublications...)
	routers = append(routers, routersHome)

	for _, route := range routers {
		if route.IsRequestAuthentication {
			r.HandleFunc(route.URI,
				middlewares.Logger(middlewares.Autenticar(route.Function)),
			).Methods(route.Method)

		} else {
			r.HandleFunc(route.URI,
				middlewares.Logger(route.Function),
			).Methods(route.Method)
		}
	}

	fileServer := http.FileServer(http.Dir("./assets/"))
	r.PathPrefix("/assets/").Handler(http.StripPrefix("/assets/", fileServer))

	return r
}
