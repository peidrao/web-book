$('#login').on('submit', (event) => {
    event.preventDefault();

    $.ajax({
        url: "/login",
        method: "POST",
        data: {
            email: $("#email").val(),
            password: $("#password").val(),
        }
    }).done(() => {
        window.location = "/home"
    }).fail((err) => {
        console.log(err)
        Swal.fire('Ops...', 'Usuário ou senha inválidos', 'error')
    })
})
