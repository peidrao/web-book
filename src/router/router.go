package router

import (
	"web-app/src/router/routers"

	"github.com/gorilla/mux"
)

func Build() *mux.Router {
	r := mux.NewRouter()
	return routers.Configure(r)
}
