package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"web-app/src/config"
	"web-app/src/cookies"
	"web-app/src/models"
	"web-app/src/requests"
	"web-app/src/responses"
	"web-app/src/utils"

	"net/http"

	"github.com/gorilla/mux"
)

func CreateUser(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	user, err := json.Marshal(map[string]string{
		"name":     r.FormValue("name"),
		"username": r.FormValue("username"),
		"email":    r.FormValue("email"),
		"password": r.FormValue("password"),
	})
	fmt.Println("USER::", user)
	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
	}
	url := fmt.Sprintf("%s/users", config.ApiURL)
	response, err := http.Post(url, "application/json", bytes.NewBuffer(user))
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
	}
	defer response.Body.Close()

	if response.StatusCode >= 400 {
		responses.CheckStatusCodeError(w, response)
		return
	}

	responses.JSON(w, response.StatusCode, response.Body)
}

func Logout(w http.ResponseWriter, r *http.Request) {
	cookies.Delete(w)
	http.Redirect(w, r, "/login", 302)
}

func SearchUsers(w http.ResponseWriter, r *http.Request) {
	nameOrUsername := strings.ToLower(r.URL.Query().Get("usuario"))

	url := fmt.Sprintf("%s/users?username=%s", config.ApiURL, nameOrUsername)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodGet, url, nil)
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}

	defer response.Body.Close()
	if response.StatusCode >= 400 {
		responses.CheckStatusCodeError(w, response)
		return
	}

	var users []models.User
	if err = json.NewDecoder(response.Body).Decode(&users); err != nil {
		responses.JSON(w, http.StatusUnprocessableEntity, responses.ErrorAPI{Error: err.Error()})
		return
	}

	utils.ExecuteTemplate(w, "users.html", users)
}

func GetProfile(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	profileID, err := strconv.ParseUint(params["profileID"], 10, 64)
	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}
	cookie, _ := cookies.Read(r)
	loggedUserID, _ := strconv.ParseUint(cookie["id"], 10, 64)

	if profileID == loggedUserID {
		http.Redirect(w, r, "/profile", 302)
		return
	}

	user, err := models.SearchUserComplete(profileID, r)

	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}

	utils.ExecuteTemplate(w, "user.html", struct {
		User         models.User
		LoggedUserID uint64
	}{
		User:         user,
		LoggedUserID: loggedUserID,
	})
}

func FollowProfile(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	profileID, err := strconv.ParseUint(params["profileID"], 10, 64)
	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/users/%d/follow", config.ApiURL, profileID)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodPost, url, nil)
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}

	defer response.Body.Close()
	if response.StatusCode >= 400 {
		responses.CheckStatusCodeError(w, response)
		return
	}

	responses.JSON(w, response.StatusCode, nil)
}

func UnFollowProfile(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	profileID, err := strconv.ParseUint(params["profileID"], 10, 64)
	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}
	url := fmt.Sprintf("%s/users/%d/unfollow", config.ApiURL, profileID)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodPost, url, nil)
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}

	defer response.Body.Close()
	if response.StatusCode >= 400 {
		responses.CheckStatusCodeError(w, response)
		return
	}

	responses.JSON(w, response.StatusCode, nil)
}

func GetProfileLogged(w http.ResponseWriter, r *http.Request) {
	cookie, _ := cookies.Read(r)
	profileID, _ := strconv.ParseUint(cookie["id"], 10, 64)
	profile, err := models.SearchUserComplete(profileID, r)

	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}

	utils.ExecuteTemplate(w, "profile.html", profile)
}

func GetPageProfileEdit(w http.ResponseWriter, r *http.Request) {
	cookie, _ := cookies.Read(r)
	profileID, _ := strconv.ParseUint(cookie["id"], 10, 64)

	channel := make(chan models.User)

	go models.SearchUserChannel(channel, profileID, r)
	profile := <-channel

	if profile.ID == 0 {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: "erro ao buscar usuário"})
		return
	}
	utils.ExecuteTemplate(w, "edit-profile.html", profile)
}

func UpdateProfile(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	user, err := json.Marshal(map[string]string{
		"name":     r.FormValue("name"),
		"username": r.FormValue("username"),
		"email":    r.FormValue("email"),
	})

	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}

	cookie, _ := cookies.Read(r)
	profileID, _ := strconv.ParseUint(cookie["id"], 10, 64)

	url := fmt.Sprintf("%s/users/%d", config.ApiURL, profileID)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodPut, url, bytes.NewBuffer(user))
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}

	defer response.Body.Close()
	if response.StatusCode >= 400 {
		responses.CheckStatusCodeError(w, response)
		return
	}

	responses.JSON(w, response.StatusCode, nil)
}

func GetPagePasswordEdit(w http.ResponseWriter, r *http.Request) {
	utils.ExecuteTemplate(w, "update-password.html", nil)
}

func UpdatePassword(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	password, err := json.Marshal(map[string]string{
		"current_password": r.FormValue("password"),
		"new_password":     r.FormValue("new_password"),
	})

	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/update_password", config.ApiURL)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodPost, url, bytes.NewBuffer(password))
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}
	defer response.Body.Close()
	if response.StatusCode >= 400 {
		responses.CheckStatusCodeError(w, response)
		return
	}

	responses.JSON(w, response.StatusCode, nil)
}

func DeleteProfile(w http.ResponseWriter, r *http.Request) {
	cookie, _ := cookies.Read(r)
	profileID, _ := strconv.ParseUint(cookie["id"], 10, 64)

	url := fmt.Sprintf("%s/users/%d", config.ApiURL, profileID)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodDelete, url, nil)
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}

	defer response.Body.Close()
	if response.StatusCode >= 400 {
		responses.CheckStatusCodeError(w, response)
		return
	}

	responses.JSON(w, response.StatusCode, nil)
}
