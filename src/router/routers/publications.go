package routers

import (
	"net/http"
	"web-app/src/controllers"
)

var routersPublications = []Route{
	{
		URI:                     "/publications",
		Method:                  http.MethodPost,
		Function:                controllers.CreatePublication,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/publications/{publicationID}/like",
		Method:                  http.MethodPost,
		Function:                controllers.LikePublication,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/publications/{publicationID}/dislike",
		Method:                  http.MethodPost,
		Function:                controllers.DislikePublication,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/publications/{publicationID}/update",
		Method:                  http.MethodGet,
		Function:                controllers.LoadindUpdateScreen,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/publications/{publicationID}",
		Method:                  http.MethodPut,
		Function:                controllers.UpdatePublication,
		IsRequestAuthentication: true,
	},
	{
		URI:                     "/publications/{publicationID}",
		Method:                  http.MethodDelete,
		Function:                controllers.DeletePublication,
		IsRequestAuthentication: true,
	},
}
