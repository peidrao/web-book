package routers

import (
	"net/http"
	"web-app/src/controllers"
)

var routersHome = Route{
	URI:                     "/home",
	Method:                  http.MethodGet,
	Function:                controllers.LoadingHome,
	IsRequestAuthentication: true,
}
