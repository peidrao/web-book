package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"web-app/src/config"
	"web-app/src/models"
	"web-app/src/requests"
	"web-app/src/responses"
	"web-app/src/utils"

	"github.com/gorilla/mux"
)

func CreatePublication(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	publication, err := json.Marshal(map[string]string{
		"title":       r.FormValue("title"),
		"description": r.FormValue("description"),
	})

	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/publications", config.ApiURL)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodPost, url, bytes.NewBuffer(publication))
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}

	defer response.Body.Close()
	if response.StatusCode >= 400 {
		responses.CheckStatusCodeError(w, response)
		return
	}
	responses.JSON(w, response.StatusCode, publication)
}

func LikePublication(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	publicationID, err := strconv.ParseUint(params["publicationID"], 10, 64)
	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/publications/%d/like", config.ApiURL, publicationID)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodPost, url, nil)
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}

	defer response.Body.Close()
	if response.StatusCode >= 400 {
		responses.CheckStatusCodeError(w, response)
		return
	}
	responses.JSON(w, response.StatusCode, nil)
}

func DislikePublication(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	publicationID, err := strconv.ParseUint(params["publicationID"], 10, 64)
	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/publications/%d/dislike", config.ApiURL, publicationID)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodPost, url, nil)
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}

	defer response.Body.Close()
	if response.StatusCode >= 400 {
		responses.CheckStatusCodeError(w, response)
		return
	}
	responses.JSON(w, response.StatusCode, nil)
}

func LoadindUpdateScreen(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	publicationID, err := strconv.ParseUint(params["publicationID"], 10, 64)
	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/publications/%d", config.ApiURL, publicationID)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodGet, url, nil)
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}
	defer response.Body.Close()
	if response.StatusCode >= 400 {
		responses.CheckStatusCodeError(w, response)
		return
	}
	var publication models.Publication
	if err = json.NewDecoder(response.Body).Decode(&publication); err != nil {
		responses.JSON(w, http.StatusUnprocessableEntity, responses.ErrorAPI{Error: err.Error()})
	}

	utils.ExecuteTemplate(w, "update_publication.html", publication)
}

func UpdatePublication(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	publicationID, err := strconv.ParseUint(params["publicationID"], 10, 64)
	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}

	r.ParseForm()
	publication, err := json.Marshal(map[string]string{
		"title":       r.FormValue("title"),
		"description": r.FormValue("description"),
	})

	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/publications/%d", config.ApiURL, publicationID)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodPut, url, bytes.NewBuffer(publication))
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}

	defer response.Body.Close()
	if response.StatusCode >= 400 {
		responses.CheckStatusCodeError(w, response)
		return
	}
	responses.JSON(w, response.StatusCode, nil)
}

func DeletePublication(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	publicationID, err := strconv.ParseUint(params["publicationID"], 10, 64)
	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/publications/%d", config.ApiURL, publicationID)
	response, err := requests.CreateRequestWithAuthentication(r, http.MethodDelete, url, nil)
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}

	defer response.Body.Close()
	if response.StatusCode >= 400 {
		responses.CheckStatusCodeError(w, response)
		return
	}
	responses.JSON(w, response.StatusCode, nil)
}
