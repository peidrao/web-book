package routers

import (
	"net/http"
	"web-app/src/controllers"
)

var routersLogin = []Route{
	{
		URI:                     "/",
		Method:                  http.MethodGet,
		Function:                controllers.LoadingScreenLogin,
		IsRequestAuthentication: false,
	},
	{
		URI:                     "/login",
		Method:                  http.MethodGet,
		Function:                controllers.LoadingScreenLogin,
		IsRequestAuthentication: false,
	},
	{
		URI:                     "/login",
		Method:                  http.MethodPost,
		Function:                controllers.Login,
		IsRequestAuthentication: false,
	},
	{
		URI:                     "/signup",
		Method:                  http.MethodGet,
		Function:                controllers.LoadingScreenSignup,
		IsRequestAuthentication: false,
	},
}
