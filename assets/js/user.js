$('#unfollow').on('click', (event) => {
    event.preventDefault()
    const profileID = $('#unfollow').data('user-id');
    $(this).prop('disabled', true);

    $.ajax({
        url: `/profile/${profileID}/unfollow`,
        method: "POST"
    }).done(function () {
        window.location = `/profile/${profileID}`;
    }).fail(function () {
        Swal.fire("Ops...", "Erro ao parar de seguir o usuário!", "error");
        $('#parar-de-seguir').prop('disabled', false);
    });
})

$('#follow').on('click', (event) => {
    event.preventDefault()
    const profileID = $('#follow').data('user-id');
    $(this).prop('disabled', true);
    $.ajax({
        url: `/profile/${profileID}/follow`,
        method: "POST"
    }).done(function () {
        window.location = `/profile/${profileID}`;
    }).fail(function () {
        Swal.fire("Ops...", "Erro ao seguir o usuário!", "error");
        $('#seguir').prop('disabled', false);
    });
})


$('#edit-user').on('submit', (event) => {
    event.preventDefault()
    $.ajax({
        url: `/edit-user`,
        method: "PUT",
        data: {
            name: $("#name").val(),
            email: $("#email").val(),
            username: $("#username").val(),
        }
    }).done(function () {
        Swal.fire('Sucesso', 'Usuário atualizado com sucesso', 'success').then(() => {
            window.location = `/profile`;
        })
    }).fail(function () {
        Swal.fire("Ops...", "Erro ao atualizar usuário!", "error");

    });
})

$('#edit-password').on('submit', (event) => {
    event.preventDefault()

    if ($("#new-password").val() != $("#confirm-new-password").val()) {
        Swal.fire("Ops...", "As senhas são diferentes!", "warning");
        return
    }
    console.log('Teste 123')

    $.ajax({
        url: `/update-password`,
        method: "POST",
        data: {
            password: $("#password").val(),
            new_password: $("#new-password").val(),
        }
    }).done(function () {
        Swal.fire('Sucesso', 'Senha atualizada com sucesso', 'success').then(() => {
            window.location = `/profile`;
        })
    }).fail(function () {

    });
})


$('#remove-account').on('click', () => {
    Swal.fire({
        title: "Atenção",
        text: "Tem certeza que deseja apagar sua conta? Essa é uma ação irreversível",
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        icon: "warning"
    }).then((confirmed) => {
        if (confirmed.value) {
            $.ajax({
                url: `/delete-password`,
                method: "DELETE",
            }).done(function () {
                Swal.fire('Sucesso', 'Usuário excluído com sucesso', 'success').then(() => {
                    window.location = '/logout'
                })
            }).fail(function () {
                Swal.fire('Sucesso', 'Usuário excluído com sucesso', 'success')
            });
        }
    })


})
