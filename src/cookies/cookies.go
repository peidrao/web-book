package cookies

import (
	"net/http"
	"time"
	"web-app/src/config"

	"github.com/gorilla/securecookie"
)

var s *securecookie.SecureCookie

func LoadingCookie() {
	s = securecookie.New(config.HashKey, config.BlockKey)
}

func Save(w http.ResponseWriter, ID, token string) error {
	data := map[string]string{
		"id":    ID,
		"token": token,
	}

	dataSecrect, err := s.Encode("dados", data)
	if err != nil {
		return err
	}

	http.SetCookie(w, &http.Cookie{
		Name:     "dados",
		Value:    dataSecrect,
		Path:     "/",
		HttpOnly: true,
	})
	return nil
}

func Read(r *http.Request) (map[string]string, error) {
	cookie, err := r.Cookie("dados")
	if err != nil {
		return nil, err
	}

	values := make(map[string]string)
	if err = s.Decode("dados", cookie.Value, &values); err != nil {
		return nil, err
	}

	return values, nil
}

func Delete(w http.ResponseWriter) {
	http.SetCookie(w, &http.Cookie{
		Name:     "dados",
		Value:    "",
		Path:     "/",
		HttpOnly: true,
		Expires:  time.Unix(0, 0),
	})
}
